using System; 
using System.ComponentModel.DataAnnotations; 
 using Microsoft.AspNetCore.Identity; 
 
namespace JingJaiPharmacy.Models {     
    public class Category {         
        public int CategoryID { get; set;}         
        public string Name {get; set;}         
        public string Explain { get; set;}     
        } 
 
    public class User :  IdentityUser{         
        public string  FirstName { get; set;}         
        public string  LastName { get; set;}     
        }

    public class Medicine {         
        public int  MedicineID { get; set; } 
        
        public int CategoryID { get; set; }         
        public Category MedCat { get; set; }  
        
        public string MedName { get; set; } 
        public int Number { get; set; }              
        public string ExplainMed { get; set; } 
        
        public string UerId {get; set;}
        public User postUser {get; set;}
    }      
} 