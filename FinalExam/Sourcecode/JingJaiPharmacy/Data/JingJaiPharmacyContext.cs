using JingJaiPharmacy.Models; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
 
namespace JingJaiPharmacy.Data {
    public class JingJaiPharmacyContext : IdentityDbContext<User>     
    {         
        public DbSet<Medicine> medList { get; set; }         
        public DbSet<Category> medCategory { get; set; }                  
             protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)         
             {             
                 optionsBuilder.UseSqlite(@"Data source= JingJaiPharmacy.db");    
              }     
      } 
} 