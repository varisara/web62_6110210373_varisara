using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using JingJaiPharmacy.Data;
using JingJaiPharmacy.Models;

namespace JingJaiPharmacy.Pages.MedicineAdmin
{
    public class IndexModel : PageModel
    {
        private readonly JingJaiPharmacy.Data.JingJaiPharmacyContext _context;

        public IndexModel(JingJaiPharmacy.Data.JingJaiPharmacyContext context)
        {
            _context = context;
        }

        public IList<Medicine> Medicine { get;set; }

        public async Task OnGetAsync()
        {
            Medicine = await _context.medList
                .Include(m => m.MedCat).ToListAsync();
        }
    }
}
