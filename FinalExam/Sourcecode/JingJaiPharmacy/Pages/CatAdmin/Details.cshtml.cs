using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using JingJaiPharmacy.Data;
using JingJaiPharmacy.Models;

namespace JingJaiPharmacy.Pages.CatAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly JingJaiPharmacy.Data.JingJaiPharmacyContext _context;

        public DetailsModel(JingJaiPharmacy.Data.JingJaiPharmacyContext context)
        {
            _context = context;
        }

        public Category Category { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = await _context.medCategory.FirstOrDefaultAsync(m => m.CategoryID == id);

            if (Category == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
