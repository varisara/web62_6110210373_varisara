using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using JingJaiPharmacy.Data;
using JingJaiPharmacy.Models;

namespace JingJaiPharmacy.Pages.CatAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly JingJaiPharmacy.Data.JingJaiPharmacyContext _context;

        public DeleteModel(JingJaiPharmacy.Data.JingJaiPharmacyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Category Category { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = await _context.medCategory.FirstOrDefaultAsync(m => m.CategoryID == id);

            if (Category == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = await _context.medCategory.FindAsync(id);

            if (Category != null)
            {
                _context.medCategory.Remove(Category);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
