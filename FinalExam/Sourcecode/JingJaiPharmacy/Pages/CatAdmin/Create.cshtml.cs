using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using JingJaiPharmacy.Data;
using JingJaiPharmacy.Models;

namespace JingJaiPharmacy.Pages.CatAdmin
{
    public class CreateModel : PageModel
    {
        private readonly JingJaiPharmacy.Data.JingJaiPharmacyContext _context;

        public CreateModel(JingJaiPharmacy.Data.JingJaiPharmacyContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Category Category { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.medCategory.Add(Category);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}